package men.ngopi.zain.myfavoritemovie;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import men.ngopi.zain.myfavoritemovie.model.Movie;
import men.ngopi.zain.myfavoritemovie.presenter.DetailPresenter;
import men.ngopi.zain.myfavoritemovie.util.Animate;
import men.ngopi.zain.myfavoritemovie.view.DetailView;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class DetailActivity extends AppCompatActivity implements DetailView {
    public static String EXTRA_DETAILS_MOVIE = "details_movie";

    @BindView(R.id.details_appbar)
    AppBarLayout appBarLayout;
    @BindView(R.id.details_collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.details_img_toolbar)
    ImageView ivToolbar;
    @BindView(R.id.details_toolbar)
    Toolbar toolbar;
    @BindView(R.id.details_img_poster)
    ImageView ivPoster;
    @BindView(R.id.details_title_expanded)
    TextView tvTitleExpanded;
    @BindView(R.id.details_sub_title_expanded)
    TextView tvSubTitleExpanded;
    @BindView(R.id.details_view_title_expanded)
    LinearLayout viewTitle;

    @BindView(R.id.details_overview)
    TextView tvOverview;
    @BindView(R.id.details_img_poster_overview)
    ImageView ivPosterOverview;
    @BindView(R.id.details_user_score)
    TextView tvUserScore;
    @BindView(R.id.details_release_date)
    TextView tvReleaseDate;
    @BindView(R.id.details_vote)
    TextView tvVote;
    @BindView(R.id.details_language)
    TextView tvLanguage;

    @BindView(R.id.layout_content_details)
    ConstraintLayout layoutDetails;
    @BindView(R.id.layout_content_details_placeholder)
    ConstraintLayout layoutDetailsPlaceHolder;

    private Movie movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ButterKnife.bind(this);

        final DetailPresenter presenter = new DetailPresenter(this);

        movie = getIntent().getParcelableExtra(EXTRA_DETAILS_MOVIE);

        layoutDetails.setVisibility(View.GONE);
        layoutDetailsPlaceHolder.setVisibility(View.GONE);
        ivPosterOverview.setVisibility(View.INVISIBLE);
        showPlaceHolder();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /* Create an Intent that will start the Menu-Activity. */
                presenter.setView(movie);
            }
        }, 400);

        // Setup Toolbar
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        collapsingToolbarLayout.setTitle("");
    }

    private void showPlaceHolder() {
        layoutDetailsPlaceHolder.setVisibility(View.VISIBLE);
        layoutDetails.setVisibility(View.GONE);
    }

    private void hidePlaceHolder() {
        layoutDetailsPlaceHolder.setVisibility(View.GONE);
        layoutDetails.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void setPoster(String poster) {
        String url = "https://image.tmdb.org/t/p/w500" + poster;

        //noinspection deprecation
        Glide.with(this)
                .load(url)
                .centerCrop()
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {

                        // Set Image Poster CollapsingTollbar
                        ivPoster.setImageDrawable(resource);

                        // Set Image Poster
                        ivPosterOverview.setImageDrawable(resource);
                    }
                });
    }

    private void setColorToolbar(String backdrop) {
        String url = "https://image.tmdb.org/t/p/w500" + backdrop;

        //noinspection deprecation
        Glide.with(this)
                .load(url)
                .centerCrop()
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {

                        // Set Image Toolbar
                        ivToolbar.setImageDrawable(resource);

                        // Set Color Palette Toolbar & StatusBar
                        Bitmap bm = ((BitmapDrawable) ivToolbar.getDrawable()).getBitmap();
                        Palette.from(bm).generate(new Palette.PaletteAsyncListener() {
                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public void onGenerated(Palette palette) {
                                int mutedColor = palette.getDarkMutedColor(R.attr.colorPrimary); //Muted, Dark, Light, Vibrant
                                collapsingToolbarLayout.setContentScrimColor(mutedColor);

                                // StatusBar
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    Window window = getWindow();
                                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                    window.setStatusBarColor(mutedColor);
                                    int flags = window.getDecorView().getSystemUiVisibility();
                                    flags &= ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                                    window.getDecorView().setSystemUiVisibility(flags);
                                }
                            }
                        });
                    }
                });

    }

    private void myAppBar(final String title) {
        // Listener AppBarLayout
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
                if (Math.abs(i) >= appBarLayout.getTotalScrollRange()) {
                    // Collapsed
                    Animate.show(ivPosterOverview, View.VISIBLE);
                    Animate.hide(ivPoster, View.GONE);
                    Animate.hide(viewTitle, View.GONE);
                    collapsingToolbarLayout.setTitle(title);
                } else {
                    // Expanded
                    Animate.hide(ivPosterOverview, View.INVISIBLE);
                    Animate.show(ivPoster, View.VISIBLE);
                    Animate.show(viewTitle, View.VISIBLE);
                    collapsingToolbarLayout.setTitle("");
                }
            }
        });
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void showMovie(Movie movie) {
        hidePlaceHolder();
        // Set Movie Name
        String movieName;
        if (movie.getTitle() == null) {
            movieName = movie.getOriginal_name();
        } else {
            movieName = movie.getTitle();
        }

        // Set Movie Date, year
        String movieYear;
        String movieDate;
        if (movie.getRelease_date() == null) {
            movieDate = movie.getFirst_air_date();
            movieYear = movieDate.substring(0, 4);
        } else {
            movieDate = movie.getRelease_date();
            movieYear = movieDate.substring(0, 4);
        }

        // Set TextView (Movie)
        tvTitleExpanded.setText(movieName);
        tvSubTitleExpanded.setText("(" + movieYear + ")");
        tvOverview.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        tvOverview.setText(movie.getOverview());
        tvUserScore.setText(String.valueOf(movie.getVote_average()));
        tvReleaseDate.setText(movieDate);
        tvVote.setText(String.valueOf(movie.getVote_count()));
        tvLanguage.setText(movie.getOriginal_language());

        // Set Poster
        setPoster(movie.getPoster_path());

        // Set Color Toolbar
        setColorToolbar(movie.getBackdrop_path());

        myAppBar(movieName);
    }
}
