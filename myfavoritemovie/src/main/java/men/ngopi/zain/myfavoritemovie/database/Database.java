package men.ngopi.zain.myfavoritemovie.database;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import men.ngopi.zain.myfavoritemovie.model.Movie;

public class Database {
    private static Database database;
    private static final String TABLE_MOVIES = "movie";
    private static final String CONTENT_AUTHORITY = "men.ngopi.zain.moviecatalogue";

    private static final Uri CONTENT_URI = new Uri.Builder().scheme("content")
            .authority(CONTENT_AUTHORITY)
            .appendPath(TABLE_MOVIES)
            .build();


    public static Database getInstance() {
        if (database == null) {
            database = new Database();
        }
        return database;
    }

    public Observable<List<Movie>> getMoviesObs(final Context context) {
        return Observable.defer(new Callable<ObservableSource<? extends List<Movie>>>() {
            @Override
            public ObservableSource<? extends List<Movie>> call() {

                List<Movie> movies = new ArrayList<>();
                Cursor cursor = context.getContentResolver().query(CONTENT_URI, null, null, null, null);

                assert cursor != null;
                if (cursor.moveToFirst()) {
                    do {
                        Movie movie = new Movie(cursor);
                        movies.add(movie);
                    } while (cursor.moveToNext());
                }

                return Observable.just(movies);
            }
        });
    }

}
