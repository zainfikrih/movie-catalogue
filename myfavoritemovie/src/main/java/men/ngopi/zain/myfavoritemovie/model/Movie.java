package men.ngopi.zain.myfavoritemovie.model;

import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

public class Movie implements Parcelable {
    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    private static final String M_ID = "id";
    private static final String M_VOTE_AVERAGE = "vote_average";
    private static final String M_VOTE_COUNT = "vote_count";
    private static final String M_TITLE = "title";
    private static final String M_RELEASE = "release_date";
    private static final String M_BACKDROP = "backdrop_path";
    private static final String M_OVERVIEW = "overview";
    private static final String M_POSTER = "poster_path";
    private static final String M_ORIGINAL_NAME = "original_name";
    private static final String M_FIRST_AIR_DATE = "first_air_date";
    private static final String M_LANGUAGE = "original_language";
    private static final String M_POPULARITY = "popularity";
    private int mId;
    private int id;
    private double vote_average;
    private int vote_count;
    private String title;
    private String release_date;
    private String backdrop_path;
    private String overview;
    private String poster_path;
    private String original_name;
    private String first_air_date;
    private String original_language;
    private double popularity;

    public Movie() {

    }

    public Movie(Cursor cursor) {
        this.id = cursor.getInt(cursor.getColumnIndexOrThrow(M_ID));
        this.vote_average = cursor.getDouble(cursor.getColumnIndexOrThrow(M_VOTE_AVERAGE));
        this.vote_count = cursor.getInt(cursor.getColumnIndexOrThrow(M_VOTE_COUNT));
        this.title = cursor.getString(cursor.getColumnIndexOrThrow(M_TITLE));
        this.release_date = cursor.getString(cursor.getColumnIndexOrThrow(M_RELEASE));
        this.backdrop_path = cursor.getString(cursor.getColumnIndexOrThrow(M_BACKDROP));
        this.overview = cursor.getString(cursor.getColumnIndexOrThrow(M_OVERVIEW));
        this.poster_path = cursor.getString(cursor.getColumnIndexOrThrow(M_POSTER));
        this.original_name = cursor.getString(cursor.getColumnIndexOrThrow(M_ORIGINAL_NAME));
        this.first_air_date = cursor.getString(cursor.getColumnIndexOrThrow(M_FIRST_AIR_DATE));
        this.original_language = cursor.getString(cursor.getColumnIndexOrThrow(M_LANGUAGE));
        this.popularity = cursor.getDouble(cursor.getColumnIndexOrThrow(M_POPULARITY));
    }

    public Movie(int id, double vote_average, String title, String release_date, String backdrop_path, String overview, String poster_path, String original_name, String first_air_date, String original_language, int vote_count, double popularity) {
        this.id = id;
        this.vote_average = vote_average;
        this.title = title;
        this.release_date = release_date;
        this.backdrop_path = backdrop_path;
        this.overview = overview;
        this.poster_path = poster_path;
        this.original_name = original_name;
        this.first_air_date = first_air_date;
        this.original_language = original_language;
        this.vote_count = vote_count;
        this.popularity = popularity;
    }

    protected Movie(Parcel in) {
        this.mId = in.readInt();
        this.id = in.readInt();
        this.vote_average = in.readDouble();
        this.vote_count = in.readInt();
        this.title = in.readString();
        this.release_date = in.readString();
        this.backdrop_path = in.readString();
        this.overview = in.readString();
        this.poster_path = in.readString();
        this.original_name = in.readString();
        this.first_air_date = in.readString();
        this.original_language = in.readString();
        this.popularity = in.readDouble();
    }

    public int getVote_count() {
        return vote_count;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public String getOriginal_name() {
        return original_name;
    }

    public String getFirst_air_date() {
        return first_air_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getVote_average() {
        return vote_average;
    }

    public String getTitle() {
        return title;
    }

    public String getRelease_date() {
        return release_date;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public String getOverview() {
        return overview;
    }

    public String getPoster_path() {
        return poster_path;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mId);
        dest.writeInt(this.id);
        dest.writeDouble(this.vote_average);
        dest.writeInt(this.vote_count);
        dest.writeString(this.title);
        dest.writeString(this.release_date);
        dest.writeString(this.backdrop_path);
        dest.writeString(this.overview);
        dest.writeString(this.poster_path);
        dest.writeString(this.original_name);
        dest.writeString(this.first_air_date);
        dest.writeString(this.original_language);
        dest.writeDouble(this.popularity);
    }
}
