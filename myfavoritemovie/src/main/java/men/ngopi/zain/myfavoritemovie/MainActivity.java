package men.ngopi.zain.myfavoritemovie;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import men.ngopi.zain.myfavoritemovie.adapter.MovieAdapter;
import men.ngopi.zain.myfavoritemovie.model.Movie;
import men.ngopi.zain.myfavoritemovie.presenter.MainPresenter;
import men.ngopi.zain.myfavoritemovie.view.MainView;

public class MainActivity extends AppCompatActivity implements MainView {

    @BindView(R.id.rv_movies)
    RecyclerView recyclerView;
    @BindView(R.id.pb_movies)
    ProgressBar pbMovies;
    @BindView(R.id.no_movies)
    ImageView ivNoMovies;

    private MovieAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        pbMovies.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        ivNoMovies.setVisibility(View.GONE);

        // Instansiasi adapter
        adapter = new MovieAdapter(this);

        // Set adapter to listview
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(50);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);

        MainPresenter presenter = new MainPresenter(this, getApplicationContext());
        presenter.getMoviesObs();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onSuccessGetMovies(List<Movie> movies) {
        if (movies.size() <= 0) {
            pbMovies.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            ivNoMovies.setVisibility(View.VISIBLE);
        } else {
            adapter.setMovies(movies);
            pbMovies.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }
}
