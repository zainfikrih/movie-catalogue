package men.ngopi.zain.myfavoritemovie.presenter;

import men.ngopi.zain.myfavoritemovie.model.Movie;
import men.ngopi.zain.myfavoritemovie.view.DetailView;

public class DetailPresenter {
    private DetailView view;

    public DetailPresenter(DetailView view) {
        this.view = view;
    }

    public void setView(Movie movie) {
        view.showMovie(movie);
    }
}
