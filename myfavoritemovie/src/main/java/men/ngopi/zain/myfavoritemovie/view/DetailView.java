package men.ngopi.zain.myfavoritemovie.view;

import men.ngopi.zain.myfavoritemovie.model.Movie;

public interface DetailView {
    void showMovie(Movie movie);
}
