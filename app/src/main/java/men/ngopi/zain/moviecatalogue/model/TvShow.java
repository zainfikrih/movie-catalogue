package men.ngopi.zain.moviecatalogue.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity
public class TvShow implements Parcelable {
    public static final Parcelable.Creator<TvShow> CREATOR = new Parcelable.Creator<TvShow>() {
        @Override
        public TvShow createFromParcel(Parcel source) {
            return new TvShow(source);
        }

        @Override
        public TvShow[] newArray(int size) {
            return new TvShow[size];
        }
    };
    @PrimaryKey(autoGenerate = true)
    private int mId;
    private int id;
    private double vote_average;
    private int vote_count;
    private String name;
    private String backdrop_path;
    private String overview;
    private String poster_path;
    private String original_name;
    private String first_air_date;
    private String original_language;
    private double popularity;

    public TvShow(int id, double vote_average, int vote_count, String name, String backdrop_path, String overview, String poster_path, String original_name, String first_air_date, String original_language, double popularity) {
        this.id = id;
        this.vote_average = vote_average;
        this.vote_count = vote_count;
        this.name = name;
        this.backdrop_path = backdrop_path;
        this.overview = overview;
        this.poster_path = poster_path;
        this.original_name = original_name;
        this.first_air_date = first_air_date;
        this.original_language = original_language;
        this.popularity = popularity;
    }

    protected TvShow(Parcel in) {
        this.id = in.readInt();
        this.vote_average = in.readDouble();
        this.vote_count = in.readInt();
        this.name = in.readString();
        this.backdrop_path = in.readString();
        this.overview = in.readString();
        this.poster_path = in.readString();
        this.original_name = in.readString();
        this.first_air_date = in.readString();
        this.original_language = in.readString();
        this.popularity = in.readDouble();
    }

    public double getPopularity() {
        return popularity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getVote_average() {
        return vote_average;
    }

    public int getVote_count() {
        return vote_count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public String getOriginal_name() {
        return original_name;
    }

    public String getFirst_air_date() {
        return first_air_date;
    }

    public String getOriginal_language() {
        return original_language;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeDouble(this.vote_average);
        dest.writeInt(this.vote_count);
        dest.writeString(this.name);
        dest.writeString(this.backdrop_path);
        dest.writeString(this.overview);
        dest.writeString(this.poster_path);
        dest.writeString(this.original_name);
        dest.writeString(this.first_air_date);
        dest.writeString(this.original_language);
        dest.writeDouble(this.popularity);
    }
}
