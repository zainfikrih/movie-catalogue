package men.ngopi.zain.moviecatalogue.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.database.Cursor;

import java.util.List;

import men.ngopi.zain.moviecatalogue.model.Movie;

@Dao
public interface MovieDao {
    @Query("SELECT * FROM movie ORDER BY popularity DESC")
    LiveData<List<Movie>> getAll();

    @Query("SELECT * FROM movie ORDER BY popularity DESC")
    List<Movie> getAllWidget();

    @Query("SELECT * FROM movie WHERE id LIKE :id LIMIT 1")
    LiveData<Movie> getMovie(int id);

    @Insert
    void insert(Movie... movies);

    @Delete
    void delete(Movie movie);

    @Query("SELECT * FROM movie ORDER BY popularity DESC")
    Cursor getAllMovie();

    @Insert
    long insertMovie(Movie movie);

    @Query("DELETE FROM movie WHERE id = :id")
    int deleteMovieById(long id);
}
