package men.ngopi.zain.moviecatalogue.view;

import java.util.List;

import men.ngopi.zain.moviecatalogue.model.Movie;

public interface MoviesView {
    void showMovies(List<Movie> movies);

    void onErrorShowMovies(String msg);

    void showProgress();

    void hideProgress();
}
