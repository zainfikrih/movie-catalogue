package men.ngopi.zain.moviecatalogue;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.widget.CompoundButton;

import com.google.gson.Gson;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import men.ngopi.zain.moviecatalogue.model.Movie;
import men.ngopi.zain.moviecatalogue.service.DailyReceiver;
import men.ngopi.zain.moviecatalogue.service.ReleaseReceiver;

public class SettingsActivity extends AppCompatActivity{

    @BindView(R.id.settings_toolbar)
    Toolbar toolbar;
    @BindView(R.id.switch_release)
    SwitchCompat switchRelease;
    @BindView(R.id.switch_daily)
    SwitchCompat switchDaily;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        final boolean daily = preferences.getBoolean("daily", false);
        boolean release = preferences.getBoolean("release", false);

        switchDaily.setChecked(daily);
        switchRelease.setChecked(release);

        switchDaily.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                DailyReceiver dailyReceiver = new DailyReceiver();

                SharedPreferences preferences = getPreferences(MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("daily", b);
                editor.apply();

                if (b) {
                    dailyReceiver.repeat(SettingsActivity.this);
                } else {
                    dailyReceiver.cancel(SettingsActivity.this);
                }
            }
        });

        switchRelease.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                ReleaseReceiver releaseReceiver = new ReleaseReceiver();

                SharedPreferences preferences = getPreferences(MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean("release", b);
                editor.apply();

                if (b) {
                    releaseReceiver.repeat(SettingsActivity.this);
                } else {
                    releaseReceiver.cancel(SettingsActivity.this);
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

}
