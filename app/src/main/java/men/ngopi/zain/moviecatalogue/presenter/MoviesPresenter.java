package men.ngopi.zain.moviecatalogue.presenter;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.support.annotation.Nullable;

import java.util.List;

import men.ngopi.zain.moviecatalogue.R;
import men.ngopi.zain.moviecatalogue.database.MovieDao;
import men.ngopi.zain.moviecatalogue.model.Movie;
import men.ngopi.zain.moviecatalogue.network.Server;
import men.ngopi.zain.moviecatalogue.view.MoviesView;

public class MoviesPresenter {
    private MoviesView view;
    private Context context;
    @SuppressLint("StaticFieldLeak")
    private static MoviesPresenter moviesPresenter;
    private MovieDao movieDao;

    public static MoviesPresenter getInstance() {
        return moviesPresenter;
    }

    public MoviesPresenter(MoviesView view, Context context, MovieDao movieDao) {
        moviesPresenter = this;
        this.view = view;
        this.context = context;
        this.movieDao = movieDao;
    }

    public void getMoviesFavorite() {
        movieDao.getAll().observeForever(new Observer<List<Movie>>() {
            @Override
            public void onChanged(@Nullable List<Movie> movies) {
                view.showMovies(movies);
            }
        });
    }

    public void getMovies() {
        view.showProgress();
        Server.getInstance(context).getMovies();
    }

    public void searchMovies(String query) {
        Server.getInstance(context).searchMovies(query);
    }

    public void onSuccessGetMovies(List<Movie> movies) {
        view.hideProgress();
        view.showMovies(movies);
    }

    public void onErrorGetMovies(String msg) {
        view.onErrorShowMovies(context.getResources().getString(R.string.error_get_data) + " " + msg);
    }

}
