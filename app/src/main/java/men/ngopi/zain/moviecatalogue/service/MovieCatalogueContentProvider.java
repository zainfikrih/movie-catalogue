package men.ngopi.zain.moviecatalogue.service;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import men.ngopi.zain.moviecatalogue.database.AppDatabase;
import men.ngopi.zain.moviecatalogue.database.MovieDao;
import men.ngopi.zain.moviecatalogue.model.Movie;

public class MovieCatalogueContentProvider extends ContentProvider {

    public static final String AUTHORITY = "men.ngopi.zain.moviecatalogue";

//    public static final Uri URI_MOVIE = Uri.parse(
//            "content://" + AUTHORITY + "/" + "movie");

    private static final int CODE_MOVIE_DIR = 1;
    private static final int CODE_MOVIE_ITEM = 2;

    private static final UriMatcher MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        MATCHER.addURI(AUTHORITY, "movie", CODE_MOVIE_DIR);
        MATCHER.addURI(AUTHORITY, "movie/#", CODE_MOVIE_ITEM);
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        Cursor cursor;
        Context context = getContext();

        if (context != null) {
            switch (MATCHER.match(uri)) {
                case CODE_MOVIE_DIR:

                    MovieDao movieDao = AppDatabase.getInstance(context).movieDao();
                    cursor = movieDao.getAllMovie();
                    cursor.setNotificationUri(context.getContentResolver(), uri);
                    return cursor;

                case CODE_MOVIE_ITEM:
                    throw new IllegalArgumentException("Invalid URI: " + uri);
                default:
                    throw new IllegalArgumentException("Unknown URI: " + uri);

            }
        } else {
            return null;
        }
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        switch (MATCHER.match(uri)) {
            case CODE_MOVIE_DIR:
                final Context context = getContext();
                if (context == null) {
                    return null;
                }
                assert contentValues != null;
                final long id = AppDatabase.getInstance(context).movieDao()
                        .insertMovie(Movie.fromContentValues(contentValues));
                context.getContentResolver().notifyChange(uri, null);
                return ContentUris.withAppendedId(uri, id);
            case CODE_MOVIE_ITEM:
                throw new IllegalArgumentException("Invalid URI, cannot insert with ID: " + uri);
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        switch (MATCHER.match(uri)) {
            case CODE_MOVIE_DIR:
                throw new IllegalArgumentException("Invalid URI, cannot update without ID" + uri);
            case CODE_MOVIE_ITEM:
                final Context context = getContext();
                if (context == null) {
                    return 0;
                }
                final int count = AppDatabase.getInstance(context).movieDao()
                        .deleteMovieById(ContentUris.parseId(uri));
                context.getContentResolver().notifyChange(uri, null);
                return count;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
