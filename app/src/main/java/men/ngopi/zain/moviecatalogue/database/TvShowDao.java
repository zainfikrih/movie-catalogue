package men.ngopi.zain.moviecatalogue.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import men.ngopi.zain.moviecatalogue.model.TvShow;

@Dao
public interface TvShowDao {
    @Query("SELECT * FROM tvshow ORDER BY popularity DESC")
    LiveData<List<TvShow>> getAll();

    @Query("SELECT * FROM tvshow WHERE id LIKE :id LIMIT 1")
    LiveData<TvShow> getTvShow(int id);

    @Insert
    void insert(TvShow... tvShows);

    @Delete
    void delete(TvShow tvShow);
}
