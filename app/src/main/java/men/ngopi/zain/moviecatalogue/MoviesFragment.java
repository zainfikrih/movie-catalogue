package men.ngopi.zain.moviecatalogue;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import men.ngopi.zain.moviecatalogue.adapter.MovieAdapter;
import men.ngopi.zain.moviecatalogue.database.AppDatabase;
import men.ngopi.zain.moviecatalogue.model.Movie;
import men.ngopi.zain.moviecatalogue.presenter.MoviesPresenter;
import men.ngopi.zain.moviecatalogue.view.MoviesView;

public class MoviesFragment extends Fragment implements MoviesView {

    @BindView(R.id.rv_movies)
    RecyclerView recyclerView;
    @BindView(R.id.pb_movies)
    ProgressBar pbMovies;
    @BindView(R.id.no_movies)
    ImageView ivNoMovies;
    @BindView(R.id.sv_fragment)
    SearchView searchView;

    private MovieAdapter adapter;
    private MoviesPresenter presenter;
    private String query = "";

    public MoviesFragment() {

    }

    public static MoviesFragment newInstance(Bundle bundle) {
        MoviesFragment moviesFragment = new MoviesFragment();
        moviesFragment.setArguments(bundle);
        return moviesFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movies, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            query = savedInstanceState.getString("query");
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        searchView.clearFocus();
    }

    private void getMovies() {
        // Save list movies
        SharedPreferences preferences = Objects.requireNonNull(getActivity()).getPreferences(Context.MODE_PRIVATE);
        if (!Objects.requireNonNull(preferences.getString("movies", "")).equalsIgnoreCase("")) {
            Gson gson = new Gson();
            Type listType = new TypeToken<List<Movie>>() {
            }.getType();
            String json = preferences.getString("movies", "");
            List<Movie> movies = gson.fromJson(json, listType);
            showMovies(movies);
        }

        // Get movies from presenter
        presenter.getMovies();
    }

    @Override
    public void onResume() {
        super.onResume();

        // Instansiasi Database
        AppDatabase database = AppDatabase.getInstance(Objects.requireNonNull(getActivity()).getApplication());

        // Instansiasi presenter
        presenter = new MoviesPresenter(this, getContext(), database.movieDao());

        // Instansiasi adapter
        adapter = new MovieAdapter(getActivity());

        // Set adapter to listview
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(50);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);

        if (getArguments() != null) {
            searchView.setVisibility(View.GONE);
            presenter.getMoviesFavorite();
        } else {
            if (searchView.getQuery().length() <= 0) {
                getMovies();
            } else {

                presenter.searchMovies(query);
                Log.i("Rotate", query);
            }
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.length() <= 0) {
                    getMovies();
                    query = s;
                } else {
                    query = s;
                    presenter.searchMovies(s);
                    recyclerView.setVisibility(View.GONE);
                    pbMovies.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("query", query);
    }

    @Override
    public void showMovies(List<Movie> movies) {
        recyclerView.setVisibility(View.VISIBLE);
        if (movies.isEmpty()) {
            ivNoMovies.setVisibility(View.VISIBLE);
        } else {
            ivNoMovies.setVisibility(View.GONE);
        }

        if (getArguments() != null) {
            pbMovies.setVisibility(View.GONE);
        } else {
            SharedPreferences preferences = Objects.requireNonNull(getActivity()).getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            Gson gson = new Gson();
            String json = gson.toJson(movies);
            editor.putString("movies", json);
            editor.apply();
        }

        // Set list movies to adapter
        adapter.setMovies(movies);

        hideProgress();

    }

    @Override
    public void onErrorShowMovies(String msg) {
//        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        // ProgressBar visible
        pbMovies.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        // ProgressBar Hide
        pbMovies.setVisibility(View.GONE);
    }

}
