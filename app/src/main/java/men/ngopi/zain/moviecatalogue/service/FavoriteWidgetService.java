package men.ngopi.zain.moviecatalogue.service;

import android.content.Intent;
import android.widget.RemoteViewsService;

public class FavoriteWidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new StackRemoteViewsFactory(this.getApplicationContext());
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
