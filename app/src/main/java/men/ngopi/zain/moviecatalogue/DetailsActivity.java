package men.ngopi.zain.moviecatalogue;

import android.annotation.SuppressLint;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import men.ngopi.zain.moviecatalogue.database.AppDatabase;
import men.ngopi.zain.moviecatalogue.model.Movie;
import men.ngopi.zain.moviecatalogue.model.TvShow;
import men.ngopi.zain.moviecatalogue.presenter.DetailsPresenter;
import men.ngopi.zain.moviecatalogue.util.Animate;
import men.ngopi.zain.moviecatalogue.view.DetailsView;

import static android.text.Layout.JUSTIFICATION_MODE_INTER_WORD;

public class DetailsActivity extends AppCompatActivity implements DetailsView {
    public static String EXTRA_DETAILS_MOVIE = "details_movie";

    @BindView(R.id.details_appbar)
    AppBarLayout appBarLayout;
    @BindView(R.id.details_collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(R.id.details_img_toolbar)
    ImageView ivToolbar;
    @BindView(R.id.details_toolbar)
    Toolbar toolbar;
    @BindView(R.id.details_img_poster)
    ImageView ivPoster;
    @BindView(R.id.details_title_expanded)
    TextView tvTitleExpanded;
    @BindView(R.id.details_sub_title_expanded)
    TextView tvSubTitleExpanded;
    @BindView(R.id.details_view_title_expanded)
    LinearLayout viewTitle;

    @BindView(R.id.details_overview)
    TextView tvOverview;
    @BindView(R.id.details_img_poster_overview)
    ImageView ivPosterOverview;
    @BindView(R.id.details_user_score)
    TextView tvUserScore;
    @BindView(R.id.details_release_date)
    TextView tvReleaseDate;
    @BindView(R.id.details_vote)
    TextView tvVote;
    @BindView(R.id.details_language)
    TextView tvLanguage;

    @BindView(R.id.layout_content_details)
    ConstraintLayout layoutDetails;
    @BindView(R.id.layout_content_details_placeholder)
    ConstraintLayout layoutDetailsPlaceHolder;

    private MenuItem item;
    private DetailsPresenter presenter;
    private Movie movie;
    private TvShow tvShow;
    private boolean isMov;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // Bind view
        ButterKnife.bind(this);

        // Instansiasi Database
        AppDatabase database = AppDatabase.getInstance(getApplication());

        layoutDetails.setVisibility(View.GONE);
        layoutDetailsPlaceHolder.setVisibility(View.GONE);
        ivPosterOverview.setVisibility(View.INVISIBLE);

        Locale locale = getResources().getConfiguration().locale;
        presenter = new DetailsPresenter(this, getApplicationContext(), database.movieDao(), database.tvShowDao());

        // Get Intent Extra
        if (getIntent().getParcelableExtra(EXTRA_DETAILS_MOVIE) instanceof Movie) {
            isMov = true;
            movie = getIntent().getParcelableExtra(EXTRA_DETAILS_MOVIE);

            if (locale.getLanguage().equalsIgnoreCase("in")) {
                // Instansiasi presenter
                showPlaceHolder();

                presenter.showMovies(movie, "id");
            } else {
                showPlaceHolder();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        /* Create an Intent that will start the Menu-Activity. */
                        presenter.showMovies(movie);
                    }
                }, 400);
            }

        } else {
            isMov = false;
            tvShow = getIntent().getParcelableExtra(EXTRA_DETAILS_MOVIE);

            if (locale.getLanguage().equalsIgnoreCase("in")) {
                // Instansiasi presenter
                showPlaceHolder();
                presenter.showTvShow(tvShow, "id");
            } else {
                showPlaceHolder();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        /* Create an Intent that will start the Menu-Activity. */
                        presenter.showTvShow(tvShow);
                    }
                }, 400);
            }

        }

        // Setup Toolbar
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        collapsingToolbarLayout.setTitle("");
    }

    private void showPlaceHolder() {
        layoutDetailsPlaceHolder.setVisibility(View.VISIBLE);
        layoutDetails.setVisibility(View.GONE);
    }

    private void hidePlaceHolder() {
        layoutDetailsPlaceHolder.setVisibility(View.GONE);
        layoutDetails.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void setPoster(String poster) {
        String url = "https://image.tmdb.org/t/p/w500" + poster;

        //noinspection deprecation
        Glide.with(this)
                .load(url)
                .centerCrop()
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {

                        // Set Image Poster CollapsingTollbar
                        ivPoster.setImageDrawable(resource);

                        // Set Image Poster
                        ivPosterOverview.setImageDrawable(resource);
                    }
                });
    }

    private void setColorToolbar(String backdrop) {
        String url = "https://image.tmdb.org/t/p/w500" + backdrop;

        //noinspection deprecation
        Glide.with(this)
                .load(url)
                .centerCrop()
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {

                        // Set Image Toolbar
                        ivToolbar.setImageDrawable(resource);

                        // Set Color Palette Toolbar & StatusBar
                        Bitmap bm = ((BitmapDrawable) ivToolbar.getDrawable()).getBitmap();
                        Palette.from(bm).generate(new Palette.PaletteAsyncListener() {
                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public void onGenerated(Palette palette) {
                                int mutedColor = palette.getDarkMutedColor(R.attr.colorPrimary); //Muted, Dark, Light, Vibrant
                                collapsingToolbarLayout.setContentScrimColor(mutedColor);

                                // StatusBar
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    Window window = getWindow();
                                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                    window.setStatusBarColor(mutedColor);
                                    int flags = window.getDecorView().getSystemUiVisibility();
                                    flags &= ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                                    window.getDecorView().setSystemUiVisibility(flags);
                                }
                            }
                        });
                    }
                });

    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void showDetailsMovies(Movie movie) {
        hidePlaceHolder();
        // Set Movie Name
        String movieName;
        if (movie.getTitle() == null) {
            movieName = movie.getOriginal_name();
        } else {
            movieName = movie.getTitle();
        }

        // Set Movie Date, year
        String movieYear;
        String movieDate;
        if (movie.getRelease_date() == null) {
            movieDate = movie.getFirst_air_date();
            movieYear = movieDate.substring(0, 4);
        } else {
            movieDate = movie.getRelease_date();
            movieYear = movieDate.substring(0, 4);
        }

        // Set TextView (Movie)
        tvTitleExpanded.setText(movieName);
        tvSubTitleExpanded.setText("(" + movieYear + ")");
        tvOverview.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        tvOverview.setText(movie.getOverview());
        tvUserScore.setText(String.valueOf(movie.getVote_average()));
        tvReleaseDate.setText(movieDate);
        tvVote.setText(String.valueOf(movie.getVote_count()));
        tvLanguage.setText(movie.getOriginal_language());

        // Set Poster
        setPoster(movie.getPoster_path());

        // Set Color Toolbar
        setColorToolbar(movie.getBackdrop_path());

        myAppBar(movieName);
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void showDetailsTvShow(TvShow tvShow) {
        hidePlaceHolder();

        String tvShowDate = tvShow.getFirst_air_date();
        String tvShowYear = tvShowDate.substring(0, 4);

        // Set TextView (Movie)
        tvTitleExpanded.setText(tvShow.getName());
        tvSubTitleExpanded.setText("(" + tvShowYear + ")");
        tvOverview.setJustificationMode(JUSTIFICATION_MODE_INTER_WORD);
        tvOverview.setText(tvShow.getOverview());
        tvUserScore.setText(String.valueOf(tvShow.getVote_average()));
        tvReleaseDate.setText(tvShowDate);
        tvVote.setText(String.valueOf(tvShow.getVote_count()));
        tvLanguage.setText(tvShow.getOriginal_language());

        // Set Poster
        setPoster(tvShow.getPoster_path());

        // Set Color Toolbar
        setColorToolbar(tvShow.getBackdrop_path());

        myAppBar(tvShow.getName());
    }

    @Override
    public void isFav(boolean isFav) {
        favorite(item, isFav);
    }

    private void myAppBar(final String title) {
        // Listener AppBarLayout
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
                if (Math.abs(i) >= appBarLayout.getTotalScrollRange()) {
                    // Collapsed
                    Animate.show(ivPosterOverview, View.VISIBLE);
                    Animate.hide(ivPoster, View.GONE);
                    Animate.hide(viewTitle, View.GONE);
                    collapsingToolbarLayout.setTitle(title);
                } else {
                    // Expanded
                    Animate.hide(ivPosterOverview, View.INVISIBLE);
                    Animate.show(ivPoster, View.VISIBLE);
                    Animate.show(viewTitle, View.VISIBLE);
                    collapsingToolbarLayout.setTitle("");
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.detail_menu, menu);
        this.item = menu.getItem(0);
        if (isMov) {
            presenter.getMovieFav(movie.getId());
        } else {
            presenter.getTvShowFav(tvShow.getId());
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_detail_favorite) {
            if (item.isChecked()) {
                if (isMov) {
                    presenter.insertMovieFav(movie);
                } else {
                    presenter.insertTvShowFav(tvShow);
                }
                favorite(item, true);
            } else {
                if (isMov) {
                    presenter.deleteMovieFav(movie);
                } else {
                    presenter.deleteTvShowFav(tvShow);
                }
                favorite(item, false);
            }

            // Update Widget Movies Favorite
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getApplicationContext());
            int[] appWidgetIds = appWidgetManager.getAppWidgetIds(
                    new ComponentName(getApplicationContext(), FavoriteWidget.class));
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.stack_view_widget);
        }
        return super.onOptionsItemSelected(item);
    }

    private void favorite(MenuItem item, boolean status) {
        if (status) {
            item.setChecked(false);
            item.setIcon(R.drawable.ic_favorite_white);
        } else {
            item.setChecked(true);
            item.setIcon(R.drawable.ic_favorite_border);
        }
    }
}
