package men.ngopi.zain.moviecatalogue.service;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import java.util.Calendar;
import java.util.List;

import men.ngopi.zain.moviecatalogue.R;
import men.ngopi.zain.moviecatalogue.model.Movie;
import men.ngopi.zain.moviecatalogue.presenter.ReleasePresenter;

import static men.ngopi.zain.moviecatalogue.service.DailyReceiver.NOTIF_CHANNEL_ID;
import static men.ngopi.zain.moviecatalogue.service.DailyReceiver.NOTIF_CHANNEL_NAME;

public class ReleaseReceiver extends BroadcastReceiver implements ReleaseInterface {

    private static int NOTIF_ID = 102;
    private static int REQ_CODE = 1001;

    private Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        ReleasePresenter presenter = new ReleasePresenter(this);
        presenter.movieReleasedToday(context);
    }


    public void repeat(Context context) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 8);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        int SDK_INT = Build.VERSION.SDK_INT;
        if (SDK_INT < Build.VERSION_CODES.KITKAT) {
            alarmManager.set(AlarmManager.RTC_WAKEUP,
                    calendar.getTimeInMillis(), pendingIntent(context));
        } else if (SDK_INT > Build.VERSION_CODES.KITKAT && SDK_INT < Build.VERSION_CODES.M) {
            alarmManager.setInexactRepeating(
                    AlarmManager.RTC_WAKEUP,
                    calendar.getTimeInMillis(),
                    AlarmManager.INTERVAL_DAY,
                    pendingIntent(context));
        } else if (SDK_INT >= Build.VERSION_CODES.M) {
            alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,
                    calendar.getTimeInMillis(), pendingIntent(context));
        }
    }

    private PendingIntent pendingIntent(Context context) {
        Intent alarmIntent = new Intent(context, ReleaseReceiver.class);
        return PendingIntent.getBroadcast(context, REQ_CODE, alarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    public void cancel(Context context) {
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(context, ReleaseReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, REQ_CODE, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager.cancel(pendingIntent);
    }

    private void showAlarmNotification(Context context, String appName, String msg, int notifId) {
        String message = String.format(context.getString(R.string.msg_upcoming), msg);

        NotificationManager notificationManager = (NotificationManager) context.getSystemService(
                Context.NOTIFICATION_SERVICE);

        Uri alarmRingtone = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, NOTIF_CHANNEL_ID)
                .setSmallIcon(R.drawable.movie_catalogue_no)
                .setContentTitle(appName)
                .setContentText(message)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(context, android.R.color.transparent))
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setSound(alarmRingtone);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIF_CHANNEL_ID, NOTIF_CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{1000, 1000, 1000, 1000, 1000});

            builder.setChannelId(NOTIF_CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        notificationManager.notify(notifId, builder.build());
    }

    @Override
    public void showReleaseMovie(List<Movie> movies) {
        for (Movie movie : movies) {

            String title;
            // Set movie name
            if (movie.getTitle() == null) {
                title = movie.getOriginal_name();
            } else {
                title = movie.getTitle();
            }

            showAlarmNotification(context, context.getString(R.string.app_name), title, NOTIF_ID);
            NOTIF_ID++;
        }
    }
}
