package men.ngopi.zain.moviecatalogue.presenter;

import android.content.Context;

import java.util.List;

import men.ngopi.zain.moviecatalogue.model.Movie;
import men.ngopi.zain.moviecatalogue.network.Server;
import men.ngopi.zain.moviecatalogue.service.ReleaseInterface;

public class ReleasePresenter {
    private static ReleasePresenter releasePresenter;
    private ReleaseInterface releaseInterface;

    public static ReleasePresenter getInstance() {
        return releasePresenter;
    }

    public ReleasePresenter(ReleaseInterface releaseInterface) {
        releasePresenter = this;
        this.releaseInterface = releaseInterface;
    }

    public void movieReleasedToday(Context context) {
        Server.getInstance(context).upcomingMovies();
    }

    public void shwoMovies(List<Movie> movies) {
        releaseInterface.showReleaseMovie(movies);
    }
}
