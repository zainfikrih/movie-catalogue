package men.ngopi.zain.moviecatalogue;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import men.ngopi.zain.moviecatalogue.adapter.TvShowAdapter;
import men.ngopi.zain.moviecatalogue.database.AppDatabase;
import men.ngopi.zain.moviecatalogue.model.TvShow;
import men.ngopi.zain.moviecatalogue.presenter.TvShowPresenter;
import men.ngopi.zain.moviecatalogue.view.TvShowView;

public class TvShowFragment extends Fragment implements TvShowView {

    @BindView(R.id.rv_movies)
    RecyclerView recyclerView;
    @BindView(R.id.pb_movies)
    ProgressBar pbMovies;
    @BindView(R.id.no_movies)
    ImageView ivNoTvShow;
    @BindView(R.id.sv_fragment)
    SearchView searchView;

    private TvShowAdapter adapter;
    private TvShowPresenter presenter;
    private String query = "";

    public static TvShowFragment newInstance(Bundle bundle) {
        TvShowFragment tvShowFragment = new TvShowFragment();
        tvShowFragment.setArguments(bundle);
        return tvShowFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movies, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (savedInstanceState != null) {
            query = savedInstanceState.getString("query");
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("query", query);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void getTvShow() {
        // Save list movies
        SharedPreferences preferences = Objects.requireNonNull(getActivity()).getPreferences(Context.MODE_PRIVATE);
        if (!Objects.requireNonNull(preferences.getString("tvShow", "")).equalsIgnoreCase("")) {
            Gson gson = new Gson();
            Type listType = new TypeToken<List<TvShow>>() {
            }.getType();
            String json = preferences.getString("tvShow", "");
            List<TvShow> tvShows = gson.fromJson(json, listType);
            Log.i("JSON Tv Show 1", json);
            showTvShow(tvShows);
        }
        // Get Tv Show from presenter
        presenter.getTvShow();
    }

    @Override
    public void onResume() {
        super.onResume();

        // Instansiasi Database
        AppDatabase database = AppDatabase.getInstance(Objects.requireNonNull(getActivity()).getApplication());

        // Instansiasi presenter
        presenter = new TvShowPresenter(this, getContext(), database.tvShowDao());

        // ProgressBar visible
        pbMovies.setVisibility(View.VISIBLE);

        // Instansiasi adapter
        adapter = new TvShowAdapter(getActivity());

        // Set adapter to listview
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(50);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);

        if (getArguments() != null) {
            searchView.setVisibility(View.GONE);
            presenter.getTvShowFavortie();
        } else {
            if (searchView.getQuery().length() <= 0) {
                getTvShow();
            } else {
                presenter.searchTvShow(query);
            }
        }

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                if (s.length() <= 0) {
                    getTvShow();
                    query = s;
                } else {
                    query = s;
                    presenter.searchTvShow(s);
                    recyclerView.setVisibility(View.GONE);
                    pbMovies.setVisibility(View.VISIBLE);
                }
                return false;
            }
        });
    }

    @Override
    public void showTvShow(List<TvShow> tvShows) {
        adapter.clearTvShows();
        recyclerView.setVisibility(View.VISIBLE);
        if (tvShows.isEmpty()) {
            ivNoTvShow.setVisibility(View.VISIBLE);
        } else {
            ivNoTvShow.setVisibility(View.GONE);
        }

        if (getArguments() != null) {
            // ProgressBar Hide
            pbMovies.setVisibility(View.GONE);

        } else {
            try {
                SharedPreferences preferences = Objects.requireNonNull(getActivity()).getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                Gson gson = new Gson();
                String json = gson.toJson(tvShows);
                editor.putString("tvShow", json);
                editor.apply();
            } catch (Exception ignored){

            }

            // ProgressBar Hide
            pbMovies.setVisibility(View.GONE);
        }

        // Set list movies to adapter
        adapter.setTvShows(tvShows);
    }

    @Override
    public void showSearch(List<TvShow> tvShows) {
        recyclerView.setVisibility(View.VISIBLE);
        if (tvShows.isEmpty()) {
            ivNoTvShow.setVisibility(View.VISIBLE);
        } else {
            ivNoTvShow.setVisibility(View.GONE);
        }

        // Set list movies to adapter
        adapter.setTvShows(tvShows);

        // ProgressBar Hide
        pbMovies.setVisibility(View.GONE);
    }

    @Override
    public void onErrorShowTvShow(String msg) {
//        Toast.makeText(getContext(), msg, Toast.LENGTH_SHORT).show();
    }
}
