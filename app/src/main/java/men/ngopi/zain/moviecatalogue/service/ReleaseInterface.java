package men.ngopi.zain.moviecatalogue.service;

import java.util.List;

import men.ngopi.zain.moviecatalogue.model.Movie;

public interface ReleaseInterface {
    void showReleaseMovie(List<Movie> movies);
}
