package men.ngopi.zain.moviecatalogue.presenter;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.support.annotation.Nullable;

import java.util.List;

import men.ngopi.zain.moviecatalogue.database.TvShowDao;
import men.ngopi.zain.moviecatalogue.model.TvShow;
import men.ngopi.zain.moviecatalogue.network.Server;
import men.ngopi.zain.moviecatalogue.view.TvShowView;

public class TvShowPresenter {
    private TvShowView view;
    private Context context;
    private TvShowDao tvShowDao;
    @SuppressLint("StaticFieldLeak")
    private static TvShowPresenter tvShowPresenter;

    public static TvShowPresenter getInstance() {
        return tvShowPresenter;
    }

    public TvShowPresenter(TvShowView view, Context context, TvShowDao tvShowDao) {
        tvShowPresenter = this;
        this.view = view;
        this.context = context;
        this.tvShowDao = tvShowDao;
    }

    public void getTvShowFavortie() {
        tvShowDao.getAll().observeForever(new Observer<List<TvShow>>() {
            @Override
            public void onChanged(@Nullable List<TvShow> tvShows) {
                view.showTvShow(tvShows);
            }
        });
    }

    public void getTvShow() {
        Server.getInstance(context).getTvShow();
    }

    public void searchTvShow(String query) {
        Server.getInstance(context).searchTvShow(query);
    }


    public void onSuccessSearchTvShow(List<TvShow> tvShows) {
        view.showSearch(tvShows);
    }


    public void onSuccessGetTvShow(List<TvShow> tvShows) {
        view.showTvShow(tvShows);
    }

    public void onErrorGetTvShow(String msg) {
        view.onErrorShowTvShow(msg);
    }

}

