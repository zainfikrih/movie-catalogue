package men.ngopi.zain.moviecatalogue.service;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Binder;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import men.ngopi.zain.moviecatalogue.R;
import men.ngopi.zain.moviecatalogue.database.AppDatabase;
import men.ngopi.zain.moviecatalogue.model.Movie;

public class StackRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

    private List<Movie> mMovies = new ArrayList<>();
    private final Context context;

    StackRemoteViewsFactory(Context context) {
        this.context = context;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public void onDataSetChanged() {
        final long identityToken = Binder.clearCallingIdentity();

        AppDatabase database = AppDatabase.getInstance(context);

        mMovies.clear();
        mMovies.addAll(database.movieDao().getAllWidget());

        Log.i("Widget", String.valueOf(mMovies.size()));

        Binder.restoreCallingIdentity(identityToken);
    }

    @Override
    public void onDestroy() {
        mMovies.clear();
    }

    @Override
    public int getCount() {
        return mMovies.size();
    }

    @Override
    public RemoteViews getViewAt(int i) {
        if (mMovies.size() <= 0) {
            return null;
        }

        RemoteViews rv = new RemoteViews(context.getPackageName(), R.layout.widget_item);

        Movie movie = mMovies.get(i);

        try {
            Bitmap bitmap = Glide.with(context)
                    .asBitmap()
                    .load("https://image.tmdb.org/t/p/w500" + movie.getPoster_path())
                    .centerCrop()
                    .submit(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                    .get();

            rv.setImageViewBitmap(R.id.iv_poster_widget_item, bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rv;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
