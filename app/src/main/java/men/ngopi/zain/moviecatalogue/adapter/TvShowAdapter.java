package men.ngopi.zain.moviecatalogue.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.card.MaterialCardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import men.ngopi.zain.moviecatalogue.DetailsActivity;
import men.ngopi.zain.moviecatalogue.R;
import men.ngopi.zain.moviecatalogue.model.TvShow;

public class TvShowAdapter extends RecyclerView.Adapter<TvShowAdapter.ViewHolder> {

    private Context context;
    private List<TvShow> tvShows;
    private LayoutInflater layoutInflater;

    public TvShowAdapter(Context context) {
        this.context = context;
        tvShows = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
    }

    public void setTvShows(List<TvShow> tvShows) {
        Log.i("Tv Adapter", Arrays.toString(tvShows.toArray()));
        this.tvShows.clear();
        this.tvShows = tvShows;
        notifyDataSetChanged();
    }

    public void clearTvShows() {
        tvShows.clear();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = layoutInflater.inflate(R.layout.item_movie, viewGroup, false);
        return new TvShowAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int i) {
        // Setup url
        String urlBackdrop = "https://image.tmdb.org/t/p/w500" + tvShows.get(i).getBackdrop_path();
        String urlPoster = "https://image.tmdb.org/t/p/w500" + tvShows.get(i).getPoster_path();

        if (tvShows.get(i).getBackdrop_path() != null) {
            // Set image backdrop
            Glide.with(context)
                    .load(urlBackdrop)
                    .apply(new RequestOptions().override(500, 200))
                    .centerCrop()
                    .timeout(3000)
                    .into(viewHolder.ivBackdrop);
        }

        if (tvShows.get(i).getPoster_path() != null) {
            // Set image psoter
            Glide.with(context)
                    .load(urlPoster)
                    .centerCrop()
                    .timeout(3000)
                    .into(viewHolder.ivPoster);
        }


        try {
            // Set tv show name
            viewHolder.tvTitle.setText(tvShows.get(i).getName());
            // Set tv show year
            String date = tvShows.get(i).getFirst_air_date();
            String year = date.substring(0, 4);
            viewHolder.tvYear.setText(year);
        } catch (Exception ignored) {

        }

        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = viewHolder.getAdapterPosition();
                Intent detailsMovieIntent = new Intent(context, DetailsActivity.class);
                detailsMovieIntent.putExtra(DetailsActivity.EXTRA_DETAILS_MOVIE, tvShows.get(position));
                context.startActivity(detailsMovieIntent);
            }
        });
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        return tvShows.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_backdrop_item_movie)
        ImageView ivBackdrop;
        @BindView(R.id.img_poster_item_movie)
        ImageView ivPoster;
        @BindView(R.id.tv_title_item_movie)
        TextView tvTitle;
        @BindView(R.id.tv_year_item_movie)
        TextView tvYear;
        @BindView(R.id.movie_card)
        MaterialCardView cardView;

        ViewHolder(@NonNull View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
