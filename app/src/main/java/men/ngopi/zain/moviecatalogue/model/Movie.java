package men.ngopi.zain.moviecatalogue.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

@Entity
public class Movie implements Parcelable {

    private static final String M_ID = "id";
    private static final String M_VOTE_AVERAGE = "vote_average";
    private static final String M_VOTE_COUNT = "vote_count";
    private static final String M_TITLE = "title";
    private static final String M_RELEASE = "release_date";
    private static final String M_BACKDROP = "backdrop_path";
    private static final String M_OVERVIEW = "overview";
    private static final String M_POSTER = "poster_path";
    private static final String M_ORIGINAL_NAME = "original_name";
    private static final String M_FIRST_AIR_DATE = "first_air_date";
    private static final String M_LANGUAGE = "original_language";
    private static final String M_POPULARITY = "popularity";
    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };
    @PrimaryKey(autoGenerate = true)
    private int mId;
    private int id;
    private double vote_average;
    private int vote_count;
    private String title;
    private String release_date;
    private String backdrop_path;
    private String overview;
    private String poster_path;
    private String original_name;
    private String first_air_date;
    private String original_language;
    private double popularity;

    public Movie() {

    }

    public Movie(int id, double vote_average, String title, String release_date, String backdrop_path, String overview, String poster_path, String original_name, String first_air_date, String original_language, int vote_count, double popularity) {
        this.id = id;
        this.vote_average = vote_average;
        this.title = title;
        this.release_date = release_date;
        this.backdrop_path = backdrop_path;
        this.overview = overview;
        this.poster_path = poster_path;
        this.original_name = original_name;
        this.first_air_date = first_air_date;
        this.original_language = original_language;
        this.vote_count = vote_count;
        this.popularity = popularity;
    }

    protected Movie(Parcel in) {
        this.mId = in.readInt();
        this.id = in.readInt();
        this.vote_average = in.readDouble();
        this.vote_count = in.readInt();
        this.title = in.readString();
        this.release_date = in.readString();
        this.backdrop_path = in.readString();
        this.overview = in.readString();
        this.poster_path = in.readString();
        this.original_name = in.readString();
        this.first_air_date = in.readString();
        this.original_language = in.readString();
        this.popularity = in.readDouble();
    }

    public static Movie fromContentValues(ContentValues values) {
        Movie movie = new Movie();
        if (values.containsKey(M_ID)) {
            movie.setId(values.getAsInteger(M_ID));
        }
        if (values.containsKey(M_VOTE_AVERAGE)) {
            movie.setVote_average(values.getAsDouble(M_VOTE_AVERAGE));
        }
        if (values.containsKey(M_VOTE_COUNT)) {
            movie.setVote_count(values.getAsInteger(M_VOTE_COUNT));
        }
        if (values.containsKey(M_TITLE)) {
            movie.setTitle(values.getAsString(M_TITLE));
        }
        if (values.containsKey(M_VOTE_COUNT)) {
            movie.setVote_count(values.getAsInteger(M_VOTE_COUNT));
        }
        if (values.containsKey(M_RELEASE)) {
            movie.setRelease_date(values.getAsString(M_RELEASE));
        }
        if (values.containsKey(M_BACKDROP)) {
            movie.setBackdrop_path(values.getAsString(M_BACKDROP));
        }
        if (values.containsKey(M_OVERVIEW)) {
            movie.setOverview(values.getAsString(M_OVERVIEW));
        }
        if (values.containsKey(M_POSTER)) {
            movie.setPoster_path(values.getAsString(M_POSTER));
        }
        if (values.containsKey(M_ORIGINAL_NAME)) {
            movie.setOriginal_name(values.getAsString(M_ORIGINAL_NAME));
        }
        if (values.containsKey(M_FIRST_AIR_DATE)) {
            movie.setFirst_air_date(values.getAsString(M_FIRST_AIR_DATE));
        }
        if (values.containsKey(M_LANGUAGE)) {
            movie.setOriginal_language(values.getAsString(M_LANGUAGE));
        }
        if (values.containsKey(M_POPULARITY)) {
            movie.setPopularity(values.getAsDouble(M_POPULARITY));
        }
        return movie;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public int getVote_count() {
        return vote_count;
    }

    public void setVote_count(int vote_count) {
        this.vote_count = vote_count;
    }

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public String getOriginal_name() {
        return original_name;
    }

    public void setOriginal_name(String original_name) {
        this.original_name = original_name;
    }

    public String getFirst_air_date() {
        return first_air_date;
    }

    public void setFirst_air_date(String first_air_date) {
        this.first_air_date = first_air_date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getVote_average() {
        return vote_average;
    }

    public void setVote_average(double vote_average) {
        this.vote_average = vote_average;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mId);
        dest.writeInt(this.id);
        dest.writeDouble(this.vote_average);
        dest.writeInt(this.vote_count);
        dest.writeString(this.title);
        dest.writeString(this.release_date);
        dest.writeString(this.backdrop_path);
        dest.writeString(this.overview);
        dest.writeString(this.poster_path);
        dest.writeString(this.original_name);
        dest.writeString(this.first_air_date);
        dest.writeString(this.original_language);
        dest.writeDouble(this.popularity);
    }
}
