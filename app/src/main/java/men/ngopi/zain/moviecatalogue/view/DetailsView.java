package men.ngopi.zain.moviecatalogue.view;

import men.ngopi.zain.moviecatalogue.model.Movie;
import men.ngopi.zain.moviecatalogue.model.TvShow;

public interface DetailsView {
    void showDetailsMovies(Movie movie);

    void showDetailsTvShow(TvShow tvShow);

    void isFav(boolean isFav);
}
