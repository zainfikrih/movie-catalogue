package men.ngopi.zain.moviecatalogue.presenter;

import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.content.Context;
import android.support.annotation.Nullable;

import men.ngopi.zain.moviecatalogue.database.MovieDao;
import men.ngopi.zain.moviecatalogue.database.TvShowDao;
import men.ngopi.zain.moviecatalogue.model.Movie;
import men.ngopi.zain.moviecatalogue.model.TvShow;
import men.ngopi.zain.moviecatalogue.network.Server;
import men.ngopi.zain.moviecatalogue.view.DetailsView;

public class DetailsPresenter {
    private DetailsView view;
    private Context context;
    @SuppressLint("StaticFieldLeak")
    private static DetailsPresenter detailsPresenter;
    private Movie movies;
    private TvShow tvShows;
    private MovieDao movieDao;
    private TvShowDao tvShowDao;

    public static DetailsPresenter getInstance() {
        return detailsPresenter;
    }

    public DetailsPresenter(DetailsView view, Context context, MovieDao movieDao, TvShowDao tvShowDao) {
        detailsPresenter = this;
        this.view = view;
        this.context = context;
        this.movieDao = movieDao;
        this.tvShowDao = tvShowDao;
    }

    public void showMovies(Movie movie) {
        view.showDetailsMovies(movie);
    }

    public void showMovies(Movie movie, String target) {
        this.movies = movie;
        Server.getInstance(context).translate(movie.getOverview(), target, movie);
    }

    public void showTvShow(TvShow tvShow) {
        view.showDetailsTvShow(tvShow);
    }

    public void showTvShow(TvShow tvShow, String target) {
        this.tvShows = tvShow;
        Server.getInstance(context).translate(tvShow.getOverview(), target, tvShow);
    }

    public void translateSuccess(String result, Object object) {
        if (object instanceof Movie) {
            movies.setOverview(result);
            view.showDetailsMovies(movies);
        } else {
            tvShows.setOverview(result);
            view.showDetailsTvShow(tvShows);
        }
    }

    public void getTvShowFav(int id) {
        tvShowDao.getTvShow(id).observeForever(new Observer<TvShow>() {
            @Override
            public void onChanged(@Nullable TvShow tvShow) {
                if (tvShow != null) {
                    view.isFav(true);
                } else {
                    view.isFav(false);
                }
            }
        });
    }

    public void insertTvShowFav(TvShow tvShow) {
        this.tvShows = tvShow;
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    tvShowDao.insert(tvShows);
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteTvShowFav(TvShow tvShow) {
        this.tvShows = tvShow;
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    tvShowDao.delete(tvShows);
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getMovieFav(int id) {
        movieDao.getMovie(id).observeForever(new Observer<Movie>() {
            @Override
            public void onChanged(@Nullable Movie movie) {
                if (movie != null) {
                    view.isFav(true);
                } else {
                    view.isFav(false);
                }
            }
        });
    }

    public void insertMovieFav(Movie movie) {
        this.movies = movie;
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    movieDao.insert(movies);
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteMovieFav(final Movie movie) {
        this.movies = movie;
        try {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    movieDao.delete(movies);
                }
            }).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
