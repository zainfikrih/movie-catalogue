package men.ngopi.zain.moviecatalogue.network;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import men.ngopi.zain.moviecatalogue.BuildConfig;
import men.ngopi.zain.moviecatalogue.model.Movie;
import men.ngopi.zain.moviecatalogue.model.TvShow;
import men.ngopi.zain.moviecatalogue.presenter.DetailsPresenter;
import men.ngopi.zain.moviecatalogue.presenter.MoviesPresenter;
import men.ngopi.zain.moviecatalogue.presenter.ReleasePresenter;
import men.ngopi.zain.moviecatalogue.presenter.TvShowPresenter;

public class Server {
    @SuppressLint("StaticFieldLeak")
    private static Server server;
    private static String apiKey = BuildConfig.TMDB_API_KEY;
    private final static String TAG = Server.class.getSimpleName();

    public static Server getInstance(Context cntx) {
        if (server == null) {
            server = new Server();
        }
        AndroidNetworking.initialize(cntx);
        return server;
    }

    // Get Movies (TMDB API)
    public void getMovies() {
        AndroidNetworking.forceCancel("get_movies");

        AndroidNetworking.get("https://api.themoviedb.org/3/discover/movie?api_key=" + apiKey)
                .setPriority(Priority.HIGH)
                .setTag("get_movies")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray jsonArray;
                        try {

                            // Response JSON
                            jsonArray = response.getJSONArray("results");

                            // Gson JSONArrayList to ArrayList<Movie>
                            Type listType = new TypeToken<List<Movie>>() {
                            }.getType();
                            List<Movie> movies = new Gson().fromJson(jsonArray.toString(), listType);

                            // Get movies success
                            MoviesPresenter.getInstance().onSuccessGetMovies(movies);

                        } catch (JSONException e) {
                            e.printStackTrace();

                            // Get movies error
                            MoviesPresenter.getInstance().onErrorGetMovies(e.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("Server", anError.getErrorBody());
                        // Get movies error
                        MoviesPresenter.getInstance().onErrorGetMovies(anError.getMessage());
                    }
                });
    }

    // Get Tv Show (TMDB API)
    public void getTvShow() {
        AndroidNetworking.forceCancel("get_tv_show");

        AndroidNetworking.get("https://api.themoviedb.org/3/discover/tv?api_key=" + apiKey)
                .setPriority(Priority.HIGH)
                .setTag("get_tv_show")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray jsonArray;
                        try {

                            // Response JSON
                            jsonArray = response.getJSONArray("results");

                            // Gson JSONArrayList to ArrayList<Movie>
                            Type listType = new TypeToken<List<TvShow>>() {
                            }.getType();
                            List<TvShow> tvShows = new Gson().fromJson(jsonArray.toString(), listType);

                            // Get movies success
                            TvShowPresenter.getInstance().onSuccessGetTvShow(tvShows);

                        } catch (JSONException e) {
                            e.printStackTrace();

                            Log.i(TAG, "Catch Show TV Show" + e.getMessage());

                            // Get movies error
                            TvShowPresenter.getInstance().onErrorGetTvShow(e.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.i(TAG, "Catch Show TV Show onError " + anError.getResponse() + " message " + anError.getErrorCode());

                        // Get movies error
                        TvShowPresenter.getInstance().onErrorGetTvShow(anError.getMessage());
                    }
                });
    }

    // Search Movies (TMDB API)
    public void searchMovies(String query) {
        AndroidNetworking.forceCancelAll();

        AndroidNetworking.get("https://api.themoviedb.org/3/search/movie?api_key=" + apiKey + "&language=en-US&query=" + query)
                .setPriority(Priority.HIGH)
                .setTag("search_movies")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray jsonArray;
                        try {

                            // Response JSON
                            jsonArray = response.getJSONArray("results");

                            // Gson JSONArrayList to ArrayList<Movie>
                            Type listType = new TypeToken<List<Movie>>() {
                            }.getType();
                            List<Movie> movies = new Gson().fromJson(jsonArray.toString(), listType);

                            // Get movies success
                            MoviesPresenter.getInstance().onSuccessGetMovies(movies);

                        } catch (JSONException e) {
                            e.printStackTrace();

                            // Get movies error
                            MoviesPresenter.getInstance().onErrorGetMovies(e.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        // Get movies error
                        MoviesPresenter.getInstance().onErrorGetMovies(anError.getMessage());
                    }
                });
    }

    // Search Tv Show (TMDB API)
    public void searchTvShow(String query) {
        AndroidNetworking.forceCancelAll();

        AndroidNetworking.get("https://api.themoviedb.org/3/search/tv?api_key=" + apiKey + "&language=en-US&query=" + query)
                .setPriority(Priority.HIGH)
                .setTag("search_tv_show")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray jsonArray;
                        try {

                            // Response JSON
                            jsonArray = response.getJSONArray("results");

                            // Gson JSONArrayList to ArrayList<Movie>
                            Type listType = new TypeToken<List<TvShow>>() {
                            }.getType();
                            List<TvShow> tvShows = new Gson().fromJson(jsonArray.toString(), listType);

                            // Get movies success
                            TvShowPresenter.getInstance().onSuccessSearchTvShow(tvShows);

                        } catch (JSONException e) {
                            e.printStackTrace();

                            // Get movies error
                            TvShowPresenter.getInstance().onErrorGetTvShow(e.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        // Get movies error
                        TvShowPresenter.getInstance().onErrorGetTvShow(anError.getMessage());
                    }
                });
    }

    // Upcoming Movies (TMDB API)
    public void upcomingMovies() {
        AndroidNetworking.get("https://api.themoviedb.org/3/movie/upcoming?api_key=" + apiKey)
                .setPriority(Priority.HIGH)
                .setTag("upcoming_movies")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray jsonArray;
                        try {

                            // Response JSON
                            jsonArray = response.getJSONArray("results");

                            // Gson JSONArrayList to ArrayList<Movie>
                            Type listType = new TypeToken<List<Movie>>() {
                            }.getType();
                            List<Movie> movies = new Gson().fromJson(jsonArray.toString(), listType);

                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                            Date date = new Date();
                            String currentDate = dateFormat.format(date);
                            Log.i("Date Now", currentDate);

                            List<Movie> moviesNow = new ArrayList<>();
                            for (Movie movie : movies) {
                                if (movie.getRelease_date().equals(currentDate)) {
                                    moviesNow.add(movie);
                                }
                            }

                            Log.i("Film Released", String.valueOf(moviesNow.size()));
                            ReleasePresenter.getInstance().shwoMovies(moviesNow);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                    }
                });
    }

    // Translate Overview Movies or Tv Show (Cloud Traslation API)
    public void translate(String query, String target, final Object object) {
        AndroidNetworking.forceCancelAll();

        AndroidNetworking.get("https://translation.googleapis.com/language/translate/v2?target=" + target + "&key=" + BuildConfig.GOOGLE_API_KEY + "&q=" + query + "")
                .setPriority(Priority.HIGH)
                .setTag("translate")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        JSONArray jsonArray;
                        try {

                            // Response JSON
                            jsonArray = response.getJSONObject("data").getJSONArray("translations");

                            String results = jsonArray.getJSONObject(0).getString("translatedText");

                            DetailsPresenter.getInstance().translateSuccess(results, object);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        anError.printStackTrace();
                    }
                });
    }

}
