package men.ngopi.zain.moviecatalogue.view;

import java.util.List;

import men.ngopi.zain.moviecatalogue.model.TvShow;

public interface TvShowView {
    void showTvShow(List<TvShow> tvShows);

    void showSearch(List<TvShow> tvShows);

    void onErrorShowTvShow(String msg);
}
